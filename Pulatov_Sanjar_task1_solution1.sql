SELECT s.store_id, CONCAT_WS(' ', s.first_name, s.last_name) AS staff_name, SUM(p.amount) AS revenue
FROM payment p
JOIN staff s ON p.staff_id = s.staff_id
JOIN store st ON s.store_id = st.store_id
WHERE p.payment_date >= '2017-01-01'::date AND p.payment_date < '2018-01-01'::date
GROUP BY st.store_id, s.staff_id
HAVING SUM(p.amount) = (
    SELECT MAX(total_revenue)
    FROM (
        SELECT st.store_id, SUM(p.amount) AS total_revenue
        FROM payment p
        JOIN staff s ON p.staff_id = s.staff_id
        JOIN store st ON s.store_id = st.store_id
        WHERE p.payment_date >= '2017-01-01'::date AND p.payment_date < '2018-01-01'::date
        GROUP BY st.store_id, s.staff_id
    ) AS revenue_by_store
    WHERE revenue_by_store.store_id = st.store_id
)
ORDER BY st.store_id;
