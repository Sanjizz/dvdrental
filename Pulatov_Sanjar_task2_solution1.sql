SELECT film.title, COUNT(rental.rental_id) AS rental_count, AVG(EXTRACT(YEAR FROM AGE(current_date, customer.create_date))) AS expected_age
FROM film
JOIN inventory ON film.film_id = inventory.film_id
JOIN rental ON inventory.inventory_id = rental.inventory_id
JOIN customer ON rental.customer_id = customer.customer_id
GROUP BY film.title
ORDER BY rental_count DESC
LIMIT 5;
