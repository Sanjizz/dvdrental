WITH ActorLastAct AS (
  SELECT
    actor.actor_id,
    actor.first_name,
    actor.last_name,
    MAX(rental.rental_date) AS last_rental_date
  FROM
    actor
    JOIN film_actor ON actor.actor_id = film_actor.actor_id
    JOIN film ON film_actor.film_id = film.film_id
    JOIN rental ON film.film_id = rental.inventory_id
  GROUP BY
    actor.actor_id, actor.first_name, actor.last_name
)

SELECT
  a.actor_id,
  a.first_name,
  a.last_name,
  EXTRACT(DAY FROM age(current_date, a.last_rental_date)) AS years_since_last_act
FROM
  ActorLastAct a
ORDER BY
  years_since_last_act DESC;
