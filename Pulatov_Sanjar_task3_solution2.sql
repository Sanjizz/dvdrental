WITH ActorLastAct AS (
  SELECT
    a.actor_id,
    a.first_name,
    a.last_name,
    MAX(r.rental_date) AS last_rental_date
  FROM
    actor AS a
    JOIN film_actor AS fa ON a.actor_id = fa.actor_id
    JOIN film AS f ON fa.film_id = f.film_id
    JOIN rental AS r ON f.film_id = r.inventory_id
  GROUP BY
    a.actor_id, a.first_name, a.last_name
)

SELECT
  a.actor_id,
  a.first_name,
  a.last_name,
  EXTRACT(DAY FROM age(current_date, a.last_rental_date)) AS years_since_last_act
FROM
  ActorLastAct AS a
ORDER BY
  years_since_last_act DESC;
