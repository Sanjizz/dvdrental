SELECT s.store_id, CONCAT(s.first_name, ' ', s.last_name) AS staff_name, SUM(p.amount) AS total_revenue
FROM payment p
INNER JOIN staff s ON p.staff_id = s.staff_id
INNER JOIN store st ON s.store_id = st.store_id
WHERE p.payment_date BETWEEN '2017-01-01' AND '2017-12-31'
GROUP BY s.store_id, s.staff_id
HAVING SUM(p.amount) = (
    SELECT MAX(revenue)
    FROM (
        SELECT s.store_id, SUM(p.amount) AS revenue
        FROM payment p
        INNER JOIN staff s ON p.staff_id = s.staff_id
        INNER JOIN store st ON s.store_id = st.store_id
        WHERE p.payment_date BETWEEN '2017-01-01' AND '2017-12-31'
        GROUP BY s.store_id, s.staff_id
    ) AS store_revenue
    WHERE store_revenue.store_id = s.store_id
)
ORDER BY s.store_id;
